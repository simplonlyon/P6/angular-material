import { Component, OnInit, ViewChild } from '@angular/core';
import { PersonService } from '../service/person.service';
import { Person } from '../entity/person';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-list-person',
  templateUrl: './list-person.component.html',
  styleUrls: ['./list-person.component.scss']
})
export class ListPersonComponent implements OnInit {
  persons:MatTableDataSource<Person>;

  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = ['id', 'name', 'surname', 'birthdate'];

  constructor(private personServ: PersonService) {
    this.persons = new MatTableDataSource();
   }

  ngOnInit() {
    this.personServ.findAll().subscribe(data => {
      this.persons.data = data;
      this.persons.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.persons.filter = filterValue.trim().toLowerCase();
  }

}

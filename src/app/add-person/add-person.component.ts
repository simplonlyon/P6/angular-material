import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PersonService } from '../service/person.service';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.scss']
})
export class AddPersonComponent implements OnInit {
  form: FormGroup;
  feedback = '';
  constructor(private fb: FormBuilder, private personServ: PersonService) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      surname: ['', [Validators.required, Validators.minLength(2)]],
      birthdate: ['', [Validators.required]]
    });
  }

  save() {
    this.personServ.add(this.form.value).subscribe(
      (person) => this.feedback = 'Person successfuly added with id ' + person.id,
      (error) => {
        this.feedback = 'There has been an error';
        console.log(error);
      }
    );
  }
}

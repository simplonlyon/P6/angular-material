import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Person } from '../entity/person';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private apiUrl = environment.backUrl + '/person';

  constructor(private http: HttpClient) { }

  findAll(): Observable<Person[]> {
    return this.http.get<Person[]>(this.apiUrl);
  }

  find(id:number):  Observable<Person> {
    return this.http.get<Person>(this.apiUrl+'/'+id);
  }

  add(person:Person):  Observable<Person> {
    person.birthdate = person.birthdate.format('YYYY-MM-DD');
    return this.http.post<Person>(this.apiUrl, person);
  }

  delete(id:number):  Observable<Person> {
    return this.http.delete<Person>(this.apiUrl+'/'+id);
  }
}
